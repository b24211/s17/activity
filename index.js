/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function firstprompt(){
		let fullname = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live?");
		console.log("Hello, " + fullname);
		console.log("You are " + age + " years old");
		console.log("You live in " + location);
		alert("Thank you for your input!");
	}
	firstprompt();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favbands(){
		let favband1 = "The Beatles";
		let favband2 = "Metallica";
		let favband3 = "The Eagles";
		let favband4 = "L'arc~en~Ciel";
		let favband5 = "Eraserheads";
		console.log("1. " + favband1);
		console.log("2. " + favband2);
		console.log("3. " + favband3);
		console.log("4. " + favband4);
		console.log("5. " + favband5);
	}
	favbands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favmovies(){
		let favmovie1 = "The Godfather", favmovie1rating = "97%";
		let favmovie2 = "The Godfather, Part II", favmovie2rating = "96%";
		let favmovie3 = "Shawshunk Redumption", favmovie3rating = "91%";
		let favmovie4 = "To Kill A Mockingbird", favmovie4rating = "93%";
		let favmovie5 = "Psycho", favmovie5rating = "96%";
		console.log("1. " + favmovie1);
		console.log("Rotten Tomatoes Rating: " + favmovie1rating);
		console.log("2. " + favmovie2);
		console.log("Rotten Tomatoes Rating: " + favmovie2rating);
		console.log("3. " + favmovie3);
		console.log("Rotten Tomatoes Rating: " + favmovie3rating);
		console.log("4. " + favmovie4);
		console.log("Rotten Tomatoes Rating: " + favmovie4rating);
		console.log("5. " + favmovie5);
		console.log("Rotten Tomatoes Rating: " + favmovie5rating);
	}
	favmovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
